﻿using FuelStationMock1.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace FuelStationMock1.Services
{
    public class FuelStationConfigService
    {

        private static string connString = WebConfigurationManager.ConnectionStrings["FsContext"].ConnectionString;

        public static void UpdateFuelStationSettings(SnackLimits model)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.FuelStationConfig_UpdateSnackLimits";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MonthSnackLimit", model.MonthSnackLimit);
                cmd.Parameters.AddWithValue("@DaySnackLimit", model.DaySnackLimit);

                cmd.ExecuteNonQuery();

                conn.Close();
            }
        }

    }
}