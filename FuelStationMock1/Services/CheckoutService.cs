﻿using FuelStationMock1.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace FuelStationMock1.Services
{
    public class CheckoutService
    {
        private static string connString = WebConfigurationManager.ConnectionStrings["FsContext"].ConnectionString;

        public static int GetTodaysCheckoutCount(int studentSportID)
        {
            int count = 0;

            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.Checkouts_GetTodaysCount";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StudentSportID", studentSportID);

                IDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    count = reader.GetInt32(0);
                }

                conn.Close();
            }

            return count;
        }

        public static CheckoutHistory GetCheckoutHistory(int studentSportID)
        {
            CheckoutHistory history = new CheckoutHistory();

            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();

                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "dbo.Checkouts_GetHistoryByStudentSportID";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StudentSportID", studentSportID);

                IDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    history.DayPreCount = reader.GetInt32(0);
                    history.DayPostCount = reader.GetInt32(1);
                    history.DayHydrationCount = reader.GetInt32(2);
                    history.DaySnacksCount = reader.GetInt32(3);
                    history.MonthSnacksCount = reader.GetInt32(4);
                }

                conn.Close();
            }

            return history;
        }
    }
}