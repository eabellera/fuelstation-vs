﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelStationMock1.Models
{
    public class SnackLimits
    {

        public int? DaySnackLimit { get; set; }
        public int? MonthSnackLimit { get; set; }
    }
}