namespace FuelStationMock1.Models.Rev2
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FsContext : DbContext
    {
        public FsContext()
            : base("name=FsContext")
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CheckoutChoice> CheckoutChoices { get; set; }
        public virtual DbSet<Choice> Choices { get; set; }
        public virtual DbSet<Checkout> Checkouts { get; set; }
        public virtual DbSet<StudentSport> StudentSports { get; set; }
        public virtual DbSet<DefaultChoice> DefaultChoices { get; set; }
        public virtual DbSet<FuelStationConfig> FuelStationConfig { get; set; }
        public virtual DbSet<SnackLimit> SnackLimits { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

        public System.Data.Entity.DbSet<FuelStationMock1.Models.Rev2.SportCode> SportCodes { get; set; }
    }
}
