﻿namespace FuelStationMock1.Models.Rev2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FuelStationConfig
    {
        public int ID { get; set; }

        [Required]
        public string Description { get; set; }
        [Required]
        public string Value { get; set; }
    }
}
