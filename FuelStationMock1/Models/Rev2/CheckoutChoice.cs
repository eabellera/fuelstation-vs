namespace FuelStationMock1.Models.Rev2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CheckoutChoice
    {
        public int CheckoutChoiceID { get; set; }

        public int CheckoutID { get; set; }

        public int ChoiceID { get; set; }

        public bool IsSnack { get; set; }


        //public virtual Checkout Checkout { get; set; }

        public virtual Choice Choice { get; set; }

        public int Type { get; set; }
    }
}
