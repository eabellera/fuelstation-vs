﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FuelStationMock1.Models.Rev2;
using System.Web.Http.Cors;

namespace FuelStationMock1.Controllers
{
    //[EnableCors(origins: "http://localhost:8100", headers: "*", methods: "*")]
    public class FuelCategoriesController : ApiController
    {
        
        private FsContext db = new FsContext();

        // GET: api/FuelCategories
        public IQueryable<Category> Get()
        {
            return db.Categories;
        }

        // GET: api/FuelCategories/5
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> Get(int id)
        {
            Category fuelCategory = await db.Categories.FindAsync(id);
            if (fuelCategory == null)
            {
                return NotFound();
            }

            return Ok(fuelCategory);
        }

        // PUT: api/FuelCategories/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, Category fuelCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fuelCategory.CategoryID)
            {
                return BadRequest();
            }

            db.Entry(fuelCategory).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FuelCategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FuelCategories
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> Post(Category fuelCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Categories.Add(fuelCategory);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApiPost", new { id = fuelCategory.CategoryID }, fuelCategory);
        }

        // DELETE: api/FuelCategories/5
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            Category fuelCategory = await db.Categories.FindAsync(id);
            if (fuelCategory == null)
            {
                return NotFound();
            }

            db.Categories.Remove(fuelCategory);
            await db.SaveChangesAsync();

            return Ok(fuelCategory);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FuelCategoryExists(int id)
        {
            return db.Categories.Count(e => e.CategoryID == id) > 0;
        }
    }
}