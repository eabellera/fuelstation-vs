﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FuelStationMock1.Models.Rev2;

namespace FuelStationMock1.Controllers
{
    public class CheckoutChoicesController : ApiController
    {
        private FsContext db = new FsContext();

        // GET: api/CheckoutChoices
        public IQueryable<CheckoutChoice> Get()
        {
            return db.CheckoutChoices;
        }

        // GET: api/CheckoutChoices/5
        [ResponseType(typeof(CheckoutChoice))]
        public async Task<IHttpActionResult> Get(int id)
        {
            CheckoutChoice checkoutChoice = await db.CheckoutChoices.FindAsync(id);
            if (checkoutChoice == null)
            {
                return NotFound();
            }

            return Ok(checkoutChoice);
        }

        // PUT: api/CheckoutChoices/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, CheckoutChoice checkoutChoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != checkoutChoice.CheckoutChoiceID)
            {
                return BadRequest();
            }

            db.Entry(checkoutChoice).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CheckoutChoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CheckoutChoices
        [ResponseType(typeof(CheckoutChoice))]
        public async Task<IHttpActionResult> Post(CheckoutChoice checkoutChoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CheckoutChoices.Add(checkoutChoice);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApiPost", new { id = checkoutChoice.CheckoutChoiceID }, checkoutChoice);
        }

        // DELETE: api/CheckoutChoices/5
        [ResponseType(typeof(CheckoutChoice))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            CheckoutChoice checkoutChoice = await db.CheckoutChoices.FindAsync(id);
            if (checkoutChoice == null)
            {
                return NotFound();
            }

            db.CheckoutChoices.Remove(checkoutChoice);
            await db.SaveChangesAsync();

            return Ok(checkoutChoice);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CheckoutChoiceExists(int id)
        {
            return db.CheckoutChoices.Count(e => e.CheckoutChoiceID == id) > 0;
        }
    }
}