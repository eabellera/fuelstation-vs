﻿fs.service('FuelService', function ($scope) {
    this.categories = [];
    this.currentCategory = {};
    var that = this;

});

fs.factory('Category', function ($resource) {
    var service = {
        categories: [],
        currentCategory: '',
        currentCategoryId : -1

    };

    service.resource = $resource('/api/FuelCategories/:id', null,
        {
            'update': { method: 'PUT' }
        });

    service.refresh = function () {
        service.categories = service.resource.query();
    }

    return service;
});

fs.factory('Choice', function ($resource) {
    var service = {
        choices: [],
        currentChoice: '',
        currentChoiceId: -1

    };

    service.getAllChoices = $resource('/api/Choices', null,
        {
            //'get': { method: 'GET', isArray: true }
        });

    service.resource = $resource('/api/FuelChoices/:id', 
        {
            id: '@choiceID'
        },
        {
            'update': { method: 'PUT' }
        });

    service.refresh = function () {
        service.choices = service.resource.query();
    }

    return service;
});

fs.factory('DefaultChoice', function ($resource) {
    var service = {
        defaultChoices: [],
        

    };

    service.resource = $resource('/api/DefaultChoices/:id', null,
        {
            'update': { method: 'PUT' }
        });

    service.getBasketResource = $resource('/api/DefaultChoices/:sportId/stage/:stageId',
        { sportId: '@sportId', stageId: '@stageId' }, {
            'get': { method:'GET', isArray: true}
        });

    service.deleteBasketResource = $resource('/api/DefaultChoices/DeleteDefaultChoicesByFuelStage/:sportId/stage/:stageId',
        { sportId: '@sportId', stageId: '@stageId' });

    service.loadDefaultResource = $resource('/api/DefaultChoices/SetDefaultChoicesByTemplate/:sportId',
        { sportId: '@sportId'
        });


    service.refresh = function () {
        service.defaultChoices = service.resource.query();

    }

    return service;
});

fs.factory('SportCode', function ($resource) {
    var service = {
        sportCodes: [],
        currentSportCode: {}

    };

    //service.realSportCodesResource = $resource('/api/SportCodes/GetRealSportCodes', null,
    //    {
    //    });

    service.resource = $resource('/api/SportCodes/:id', null,
        {
            'update': { method: 'PUT' }
        });

    service.refresh = function () {
        service.sportCodes = service.resource.query();

    }

    
    return service;
});

fs.factory('Report', function ($resource, $http) {
    var service = {
        dailyCheckouts: []
    };

    service.dailyResource = $resource('/api/Checkouts/GetDailyCheckouts');
    service.dailyCheckouts = service.dailyResource.query();

    return service;
});

fs.factory('Checkout', function ($resource, $http) {
    var service = {
        checkouts: [],
        currentCheckout: {},
        currentCheckoutIndex: -1,
        currentChoiceIndex: -1,

        archives: [],
        currentArchive: {},
        currentArchiveIndex: {}
    };


    service.resource = $resource('/api/Checkouts/:id', null,
        {
            'update': { method: 'PUT' }
        });


    service.getArchives = function () {
        $http.get('/api/Checkouts/archives').
            success(function (data) {
                service.archives.length = 0;
                service.archives.push.apply(service.archives, data);
            });
    }

    service.getPastCheckoutResource = $resource('/api/Checkouts/GetPastCheckouts/:studentId',
        { studentId: '@studentId' }, {
            'get': { method: 'GET', isArray: true }
        });

    service.refresh = function () {
        service.checkouts = service.resource.query();
        }
    

    service.refreshChoices = function () {
        tempSvc = new $resource('/api/CheckoutChoices/:id', { id: "@id" });
        currentCheckout = tempSvc.get({ id: service.currentCheckout.checkoutID });
    }

    return service;
});

fs.factory('checkoutChoiceRepository', function ($resource) {
    return $resource('/api/CheckoutChoices/:id', { id: "@id" })
});

fs.factory('Athlete', function ($resource, $cacheFactory) {
    var service = {

    };

    service.resource = $resource('/api/StudentSport/:schoolsidnumber', { schoolsidnumber: "@id" }, {
        'get': { method: 'GET', cache: $cacheFactory, isArray: true },
        'query': { method: 'GET', cache: $cacheFactory, isArray: true }
    });

    service.getAthlete = function (id) {
        return service.resource.get({ schoolsidnumber: id });
    }

    return service;

});

fs.factory('FSConfig', function ($resource, $cacheFactory) {
    var service = {

    };

    service.snackLimits = $resource('/api/FuelStationConfig/SnackLimits', null, 
        {
            'update': { method: 'PUT' }
        }
    );

    return service;

});

